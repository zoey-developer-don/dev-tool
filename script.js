/**
 * Pixafy
 *
 * @category    Development Tools
 * @package     pix.PixDevTool
 * @copyright   Copyright (c) 2014 Pixafy (http://www.pixafy.com)
 * @author      Don Desroches <ddesroches@pixafy.com>
 *
 * TODO:
 *  - Add container dragability
 *  - Add ability to show description and author data as tooltip
 */

;(function( win, doc, undefined ){
	"use strict";

	var DevTool = function () {
		var self = this;
		/**
		 * Checks to see if the DevTool was already build and exists on the page before build.
		 */
		this.init = function () {
			this.modules = [];
			this.body = doc.body;
			this.head = doc.getElementsByTagName('head')[0];
			this.container = doc.getElementById('pixafy-dev-tool-container');

			if(this.container === null){
				this.placeStyles();
				this.placeScript();
				this.events();
			}
		};

		/**
		 * Adds modules script and DevTool container to the body.
		 */
		this.placeScript = function() {
			var script = doc.createElement('SCRIPT');
			script.id = "pixafy-dev-tool-modules-script";
			script.type = "text/javascript";
			script.src = '//happy.pixafy.com/devtool/modules.js?'+this.getCurrentTime();
			this.body.appendChild(script);
			this.container = this.createContainer();
			this.body.appendChild(this.container);
		};

		/**
		 * Adds styles to the body.
		 */
		this.placeStyles = function() {
			var styles = doc.createElement('LINK');
			styles.id = "pixafy-dev-tool-styles";
			styles.href = "//happy.pixafy.com/devtool/styles.css?"+this.getCurrentTime();
			styles.rel = "stylesheet";
			styles.type = "text/css";
			styles.media = "all";
			this.head.appendChild(styles);
		};

		/**
		 * Returns current time.
		 */
		this.getCurrentTime = function(){
			return new Date().getTime();
		};

		/**
		 * Adds general events to the DevTool Module.
		 */
		this.events = function() {
			// Add remove script to close button
			var button = doc.getElementById('pixafy-dev-tool-close-btn');
			button.onclick = this.removeScript;

			// Add opacity on hover events
			this.container.onmouseover = this.removeOpacity;
			this.container.onmouseout = this.addOpacity;
		};

		/**
		 * Removes the DevTool Container, Styles, & Script Nodes.
		 */
		this.removeScript = function(){
			var DevToolContainer = doc.getElementById('pixafy-dev-tool-container');
			var DevToolStyles = doc.getElementById('pixafy-dev-tool-styles');
			var DevToolScript = doc.getElementsByTagName('script');
			var l = DevToolScript.length;

			self.body.removeChild(DevToolContainer);
			self.head.removeChild(DevToolStyles);
			self.body.removeChild(DevToolScript[l-1]);
		};

		/**
		 * Makes the DevTool container see though.
		 */
		this.addOpacity = function(){
			this.style.opacity = '0.3';
		};

		/**
		 * Makes the DevTool container opaque.
		 */
		this.removeOpacity = function(){
			this.style.opacity = '1';
		};

		/**
		 * Creates the DevTool Container.
		 * @return container
		 */
		this.createContainer = function() {
			// 
			var el = doc.createElement("DIV");
			el.id = 'pixafy-dev-tool-container';

			var h1 = this.createTitle('h1','Pixafy Dev Tools');
			el.appendChild(h1);

			var btn = this.createCloseBtn();
			el.appendChild(btn);

			return el;
		};

		/**
		 * Creates the DevTool Title
		 * @return h1
		 */
		this.createTitle = function(tag, title){
			var h1 = doc.createElement(tag);
			var titleText = doc.createTextNode(title);
			h1.appendChild(titleText);
			return h1;
		};

		/**
		 * Creates the DevTool close button
		 * @return button
		 */
		this.createCloseBtn = function(){
			var button = doc.createElement('DIV');
			var btnText = doc.createTextNode('X');
			button.id = 'pixafy-dev-tool-close-btn';
			button.appendChild(btnText);
			return button;
		};

		/**
		 * Creates module button
		 * @return button
		 */
		this.createButton = function(title, script){
			var text = doc.createTextNode(title);
			var button = doc.createElement('BUTTON');
			button.className = "pixafy-dev-tool-module-button"
			button.appendChild(text);
			button.onclick = script;

			return button;
		};

		/**
		 * Creates parent container for module
		 * @return button
		 */
		this.createParent = function(parent){
			if(this.modules[parent] === undefined){
				//Creates parent title
				var h3 = this.createTitle('h3', parent);

				//Creates parent container
				var div = doc.createElement('DIV');
				div.className = "pixafy-dev-tool-parent-block";
				div.id = "pixafy-dev-tool-parent-"+parent;

				//Creates parent list container
				var ul = doc.createElement("UL");

				div.appendChild(h3);
				div.appendChild(ul);
				this.placeEl(div);
			}
		};

		/**
		 * Places the element into the DevTool container.
		 */
		this.placeEl = function(module){
			this.container.appendChild(module);
		};

		/**
		 * Adds the child module to the parent list.
		 */
		this.appendToParent = function(title, module){
			var div = doc.getElementById('pixafy-dev-tool-parent-'+title);
			var ul = div.getElementsByTagName("UL")[0];
			var li = doc.createElement("LI");
			li.className = "pixafy-dev-tool-child";
			li.appendChild(module);
			ul.appendChild(li);
		};

		//Add Dev Tool Module to the Dev Tool container	
		this.addModule = function( arg ){
			var script, parent, title, defaults = {
				title: 'Script Title',
				parent: 'General',
				script: ''
			};
			title = defaults.title;
			parent = defaults.parent;
			script = defaults.script;

			if(arg.hasOwnProperty('title'))
				title = arg.title;
				
			if(arg.hasOwnProperty('parent'))
				parent = arg.parent;

			if(arg.hasOwnProperty('script'))
				script = arg.script;

			var module = self.createButton(title, script);

			self.createParent(parent);
			self.appendToParent(parent, module);
			self.modules[parent] = module;

			return {
				title: title,
				parent: parent,
				script: script
			};
		}

		this.init();

		return {
			addModule: this.addModule
		};
	};

	win.pix = win.pix || {};
	win.pix.PixDevTool = new DevTool();
	win.pix.PixDevTool.modules = [];

})(this, this.document);