;(function( win, doc, undefined ){
	"use strict";

	/**
	 * Check jQuery Module
	 * This will check to see if jQuery exists on the page. If it does it will report the version number.
	 */
	var checkJQuery = function(){
		return function(){
			if(win.jQuery !== undefined){
				var version = jQuery.fn.jquery;;
				alert("This site is currently using jQuery version "+version)
			} else {
				alert("This site isn't using jQuery.");
			}
		};
	};

	win.pix.PixDevTool.modules.push(win.pix.PixDevTool.addModule({
		title: "jQuery Check",
		script: checkJQuery()
	}));

	//Check jQuery Module End

	/**
	 * Fill Out Magento Checkout Form Module
	 * This will fill out the Billing, Shipping, and Payment form of the Magento checkout.
	 * @step Will accept 'billing', 'shipping', or 'payment'
	 */ 
	var checkoutBillingShippingPayment = function(step){
		return function(){
			var obj = {
				'[firstname]':'Pix',
				'[lastname]': 'Dev',
				'[street][]': ['810 7th Ave', '37th Floor'],
				'[city]': 'New York',
				'[region_id]':43,
				'[postcode]':10019,
				'[telephone]':1111111111,
				'[email]': 'fakeemail@pixafy.com',
				'[cc_type]': 'VI',
				'[cc_exp_month]':5,
				'[cc_exp_year]': 2017,
				'[cc_number]': 4111111111111111,
				'[cc_cid]': 123
			};

			for (var key in obj){
				var el = document.getElementsByName(step+key);
				if(obj.hasOwnProperty(key) && el != null){
					var i=0,l=el.length;
					for(i;i<l;i++){
						if(Object.prototype.toString.call( obj[key] )==='[object Array]'){
							el[i].value = obj[key][i];
						} else {
							el[i].value=obj[key];
						}
					}
				}
			}
		}
	};

	win.pix.PixDevTool.modules.push(win.pix.PixDevTool.addModule({
		title: "Billing Form",
		parent: "Magento Checkout",
		script: checkoutBillingShippingPayment('billing')
	}));

	win.pix.PixDevTool.modules.push(win.pix.PixDevTool.addModule({
		title: "Shipping Form",
		parent: "Magento Checkout",
		script: checkoutBillingShippingPayment('shipping')
	}));

	win.pix.PixDevTool.modules.push(win.pix.PixDevTool.addModule({
		title: "Payment Form",
		parent: "Magento Checkout",
		script: checkoutBillingShippingPayment('payment')
	}));
	// Fill Out Magento Checkout Form Module End

	/**
	 * List Objects on Pix Module
	 * This will list out all the object that live on the global pix object.
	 */
	var listPix = function(){
		return function(){
			var result = [];
			if(win.pix != undefined){
				for(var key in win.pix){
					if(win.pix.hasOwnProperty(key) && key != 'PixDevTool'){
						result.push(" - "+key);
					}
				}
				if(!result.length)
					result = "This site isn't using any Pixafy JavaScript modules."
				else
					result = 'This site is currently using the following javascript modules:\n'+result.join('\n');
			}
			alert(result);
		};
	};

	win.pix.PixDevTool.modules.push(win.pix.PixDevTool.addModule({
		title: "List Pix JS Modules",
		parent: "Pixafy",
		script: listPix()
	}));

	/**
	 * Window Instance Checker Module
	 * This will alert the number of objects that live directly on the global window object.
	 */
	var instanceChecker = function(){
		return function(){
			var object = window;
			var count = 0;
			for(var prop in object){
				if(object[prop] instanceof Object && !object[prop].prototype){
					count++;
				}
			}
			alert("There are "+count+" instances directly on the 'window' object.");
		}
	};

	win.pix.PixDevTool.modules.push(win.pix.PixDevTool.addModule({
		title: "Window Instance Checker",
		script: instanceChecker()
	}));

})(this, this.document);