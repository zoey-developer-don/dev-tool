# DevTool README #

This tool is used in Magento development to provide assistance wherever needed. It is completely modular and provides an addModule function to extend the functionality.

## Where To Start
To use this module. Simply copy and paste the code below as a bookmarklet in any browser. Then to use, just click on the newly made bookmark. All Modules will be added to the page in a user friendly interface.


```
#!javascript

javascript:(function(e,t,n){"use strict";var r=t.createElement("script");var i=(new Date).getTime();r.src="http://happy.pixafy.com/devtool/script.js?"+i;r.type="text/javascript";r.id="pixafy-dev-tool-script";t.body.appendChild(r)})(this,this.document);
```

## Add A Module
To add a module, simply fork the project. Create a new branch off of master specific to your module. Append whatever functionality you would like to see by adding the following code to the bottom of the file.

Define the functionality you'd like to see in a function. Then use the addModule function on the pix.PixDevTool object with the desired content. then push it the the pix.PixDevTool.modules array.

```
#!javascript

win.pix.PixDevTool.modules.push(win.pix.PixDevTool.addModule({
	title: "Title of Button", //Used as the button copy. If left blank will default to 'Script Title'.
	parent: "Parent Group", //Used to group like modules together. If left blank will default to 'General'.
	script: function(){ //The function you're looking to execute on click. If left blank will default to ''.
			Do things in here
		}
	} 
}));
```